/* Author:http://www.raiantspace.com

*/
jQuery(document).ready(function(){

	//IMAGE FIT
	jQuery(".imgFill").imgLiquid({fill:true});

	//PLACEHOLDER
	jQuery.placeholder.shim();

	//RESPONSIVE NAV
	var navigation = responsiveNav(".navigation", {
        animate: true,        // Boolean: Use CSS3 transitions, true or false
        transition: 250,      // Integer: Speed of the transition, in milliseconds
        label: "Menu",        // String: Label for the navigation toggle
        insert: "after",      // String: Insert the toggle before or after the navigation
        customToggle: "",     // Selector: Specify the ID of a custom toggle
        openPos: "relative",  // String: Position of the opened nav, relative or static
        jsClass: "js",        // String: 'JS enabled' class which is added to <html> el
        init: function(){},   // Function: Init callback
        open: function(){},   // Function: Open callback
        close: function(){}   // Function: Close callback
      });

	//SEVEN SLIDER
	var tb=jQuery(".seven_container").superseven({
			width:1170,
			height:456,
			autoplay:true,
			interval:7,
			fullwidth:true,
			responsive:true,
			progressbar:true,
			progressbartype:'linear',
			swipe:false,
			keyboard:false,
			scrollmode:false,
			animation:16,
			caption_animation: 2,
			bullets:false,
			thumbnail:false,
			repeat_mode:true,
			skin:'fullwidth',
			lightbox:false,
			pause_on_hover:false,
			onanimstart:function(){
				animation:1
			},
			onanimend:function(){
				animation:86
			},
			onvideoplay:function(){},
			onslidechange:function(){}		
	});	


	//jQuery.removeClass("seven_current");

	//SMOOTHSCROLL
	  jQuery('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

	      var target = jQuery(this.hash);
	      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        jQuery('html,body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });

	  //BXSLIDER
	  jQuery('#sl-slider').bxSlider({
  		mode: 'vertical',
  		slideMargin: 5,
  		controls: false,
  		adaptiveHeight: true,
	 });

	  jQuery('#bxslidev').bxSlider({
  		//mode: 'vertical',
  		slideMargin: 5,
  		controls: true,
  		pager: false
  		//adaptiveHeight: true,
	 });	

	 //SELECT
	 jQuery(".select_id").selectbox(); 

	//CHECKBOX and RADIO
	jQuery('input[type=radio], input[type=checkbox]').customRadioCheck();

	//BOOTSRAP MODAL
	jQuery('#myModal').modal('toggle');

	jQuery('.widget-filter h4 > a.open-cat-box').click(function(e) {
		e.preventDefault();
		jQuerythis = jQuery(this);
		jQueryParentTarget = jQuerythis.parent().parent();
		if( ! (jQueryParentTarget.hasClass('current-cat') || jQueryParentTarget.hasClass('current-cat-parent') ) ){
			jQuerytarget = jQuerythis.parent().next();
			if (!jQuerytarget.hasClass('active')) {
		//			allPanels.removeClass('active').slideUp();
				jQuerytarget.addClass('active').slideDown();
			}else{
				jQuerytarget.removeClass('active').slideUp();
			}
		}
		
	});

	jQuery("#ticketview input[type=checkbox]").switchButton({
	  width: 44,
	  height: 23,
	  button_width: 24
	});



	//TOGGLE
	jQuery('.wdg-sidenav a.item').click(function (e){
		e.preventDefault();
		var jtarget = jQuery(this).next('ul');
		if (!jtarget.hasClass('open')) {
			jtarget.addClass('open').slideDown(100);
		}else{
			jtarget.removeClass('open').slideUp("fast");
		}
	});

	jQuery(window).on("load", function(){
		var wHe = jQuery(".boxthin").height();

		jQuery('.shadow').css({
					'height': wHe + 200
				});

	});

	jQuery("#box_val li").hover(function(){
		jTarget = jQuery("#box_val li"); 
		jTarget.removeClass("active");
		jQuery(this).addClass("active");

		jTarget_text= jQuery(this).text();
		jQuery("#box_val_input").val(jTarget_text);
	});

	var halfHeight = jQuery('.postLetter').height() / 1.5;
	jQuery(window).load(function(){
		jQuery('.postLetter').css('height', halfHeight);
		jQuery('.letterExpand').click(function(){
			jQuery(this).closest('.postLetter').toggleClass('show');
			if(jQuery('.postLetter').hasClass('show')){
				jQuery(this).text('hide letter');
			}else{
				jQuery(this).text('show letter');
			}
			
		});
	});
	
});



