jQuery(document).ready(function() {
	  var jQueryfilterType = jQuery('#filterOptions li.active a').attr('class');
	  var jQueryholder = jQuery('ul.ourHolder');
	  var jQuerydata = jQueryholder.clone();
		jQuery('#filterOptions li a').click(function(e) {
			jQuery('#filterOptions li').removeClass('active');

			var jQueryfilterType = jQuery(this).attr('class');
			jQuery(this).parent().addClass('active');
			
			if (jQueryfilterType == 'all') {
				var jQueryfilteredData = jQuerydata.find('li');
			} 
		else {

			var jQueryfilteredData = jQuerydata.find('li[data-type=' + jQueryfilterType + ']');
		}
		jQueryholder.quicksand(jQueryfilteredData, {
			duration:600,
			easing: 'easeInOutQuad'
		});
		return false;
	});
});