;(function(){
jQuery.fn.customRadioCheck = function() {

  return this.each(function() {

    var jQuerythis = jQuery(this);
    var jQueryspan = jQuery('<span/>');

    jQueryspan.addClass('custom-'+ (jQuerythis.is(':checkbox') ? 'check' : 'radio'));
    jQuerythis.is(':checked') && jQueryspan.addClass('checked'); // init
    jQueryspan.insertAfter(jQuerythis);

    jQuerythis.parent('label').addClass('custom-label')
      .attr('onclick', ''); // Fix clicking label in iOS
    // hide by shifting left
    jQuerythis.css({ position: 'absolute', left: '-9999px' });

    // Events
    jQuerythis.on({
      change: function() {
        if (jQuerythis.is(':radio')) {
          jQuerythis.parent().siblings('label')
            .find('.custom-radio').removeClass('checked');
        }
        jQueryspan.toggleClass('checked', jQuerythis.is(':checked'));
      },
      focus: function() { jQueryspan.addClass('focus'); },
      blur: function() { jQueryspan.removeClass('focus'); }
    });
  });
};
}());